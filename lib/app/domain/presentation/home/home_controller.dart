import 'package:dogs_lover/app/data/repository/api_repository.dart';
import 'package:dogs_lover/app/data/repository/local_storage_repository.dart';
import 'package:dogs_lover/app/domain/model/breed_detail_model.dart';
import 'package:dogs_lover/app/domain/model/breed_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class HomeController extends GetxController {
  HomeController({
    this.localStorageInterface,
    this.apiInterface,
  });

  final LocalStorageRepoInterface localStorageInterface;
  final ApiRepoInterface apiInterface;

  RxList<BreedModel> _listBreeds = <BreedModel>[].obs;

  RxList<BreedModel> listBreeds = <BreedModel>[].obs;
  RxList<String> listFavoriteBreed = <String>[].obs;
  Rx<BreedDetailModel> breedCurrent = BreedDetailModel().obs;

  RxDouble widthMenu = 0.0.obs;
  RxDouble widthSearchButton = 40.0.obs;
  RxBool isSearchButtonExpanded = false.obs;
  RxBool isMenuExpanded = true.obs;
  RxBool isLoading = false.obs;

  final searchTextController = TextEditingController();

  @override
  void onInit() {
    widthMenu.value = Get.width * .25;
    super.onInit();
  }

  @override
  void onReady() async {
    await getFavoriteList();
    searchTextController.addListener(searchBreed);
    getAllBreeds();
    super.onReady();
  }

  getAllBreeds() {
    apiInterface.getListAllBreeds().then((response) {
      listBreeds.clear();
      response.message.forEach((key, value) {
        listBreeds.add(new BreedModel(
          breedName: key,
          breeds: value,
          breedFavorite: isFavoriteBreed(key),
        ));
        _listBreeds.add(new BreedModel(
          breedName: key,
          breeds: value,
          breedFavorite: isFavoriteBreed(key),
        ));
      });
    });
  }

  addfavoriteBreed(String breedName) async {
    bool status = await localStorageInterface.saveFavoriteBreed(breedName);
    if (status) {
      int idx = _listBreeds.indexWhere((breed) => breed.breedName == breedName);

      if (idx != -1) {
        listFavoriteBreed.add(breedName);
        _listBreeds[idx].breedFavorite = true;

        listBreeds.clear();
        listBreeds.addAll(_listBreeds);
      }
    }
  }

  removefavoriteBreed(String breedName) async {
    bool status = await localStorageInterface.removeFavoriteBreed(breedName);
    if (status) {
      int idx = _listBreeds.indexWhere((breed) => breed.breedName == breedName);

      if (idx != -1) {
        listFavoriteBreed.removeWhere((breed) => breed == breedName);
        _listBreeds[idx].breedFavorite = false;

        listBreeds.clear();
        listBreeds.addAll(_listBreeds);
      }
    }
  }

  getFavoriteList() async {
    listFavoriteBreed.clear();
    var _list = await localStorageInterface.getListFavoritesBreeds();
    listFavoriteBreed.addAll(_list);
  }

  setBreedSelected(String breedName) async {
    isLoading(true);
    BreedDetailModel breedDetailModel = BreedDetailModel();
    breedDetailModel.breedName = breedName;
    breedDetailModel.breedsImages = [];
    await apiInterface.getListFromBreedName(breedName).then((response) {
      if (response.breedsImages != null) {
        for (int i = 0; i < response.breedsImages.length; i++) {
          if (i < 5) {
            breedDetailModel.breedsImages.add(response.breedsImages[i]);
          }
        }
      }
    });
    isLoading(false);
    breedCurrent.value = breedDetailModel;
  }

  searchBreed() {
    final searchTerm = searchTextController.text;
    List<BreedModel> filterList = [];

    listBreeds.clear();
    listBreeds.addAll(_listBreeds);
    if (searchTerm.isEmpty) {
      return;
    } else {
      listBreeds.forEach((breed) {
        if (breed.breedName.contains(searchTerm)) {
          filterList.add(breed);
        }
      });
      listBreeds.clear();
      listBreeds.addAll(filterList);
    }
  }

  changeSearchMenuWidth(double newValue) {
    if (newValue < 41) {
      isSearchButtonExpanded(true);
      widthSearchButton(200);
    } else {
      isSearchButtonExpanded(false);
      widthSearchButton(40.0);
    }
  }

  bool isFavoriteBreed(String breedName) {
    int index = listFavoriteBreed.indexWhere((element) => element == breedName);
    if (index != -1) {
      return true;
    } else {
      return false;
    }
  }

  void changeMenuWidth(double newValue) {
    widthMenu.value = newValue;
  }

  void stateLoading(bool newValue) {
    isLoading.value = newValue;
  }

  void stateMenuExpanded(bool newValue) {
    isMenuExpanded.value = newValue;
  }
}
