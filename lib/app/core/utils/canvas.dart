import 'dart:ui';

import 'package:dogs_lover/app/core/utils/colors.dart';
import 'package:flutter/material.dart';

class RPSCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = new Paint()
      ..color = ColorDogsLover.orange
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;

    Path path = Path();
    path.moveTo(size.width * 0.0, size.height * 0.0);
    path.quadraticBezierTo(
      size.width * 0.1,
      size.height * 0.05,
      size.width * 0.3,
      size.height * 0.1,
    );
    path.quadraticBezierTo(
      size.width * 0.6,
      size.height * 0.2,
      size.width * 0.6,
      size.height * 0.6,
    );
    path.lineTo(size.width * 0.58, size.height * 0.53);
    path.quadraticBezierTo(
      size.width * 0.54,
      size.height * 0.75,
      size.width * 0.35,
      size.height * 0.8,
    );
    path.quadraticBezierTo(
      size.width * 0.0,
      size.height * 0.95,
      size.width * 0.0,
      size.height * 1.0,
    );
    path.lineTo(size.width * 0.0, size.height * -0.0);
    path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
