import 'package:dogs_lover/app/data/repository/api_repository.dart';
import 'package:dogs_lover/app/domain/model/breed_list_model.dart';
import 'package:dogs_lover/app/domain/model/breed_detail_model.dart';

class ApiRepositoryMock extends ApiRepoInterface {
  @override
  Future<BreedListModel> getListAllBreeds() async {
    BreedListModel _bredList = BreedListModel();

    var data = {
      "message": {
        "affenpinscher": [],
        "african": [],
        "appenzeller": [],
        "australian": ["shepherd"],
        "basenji": [],
        "briard": [],
        "buhund": ["norwegian"],
        "bulldog": ["boston", "english", "french"],
        "bullterrier": ["staffordshire"],
      },
      "status": "success"
    };

    _bredList = BreedListModel.fromJson(data);

    return _bredList;
  }

  @override
  Future<BreedDetailModel> getListFromBreedName(String breedName) {
    throw UnimplementedError();
  }
}
