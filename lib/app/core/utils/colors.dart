import 'package:flutter/material.dart';

class ColorDogsLover {
  static const Color orange = Color(0xFFE16937);
  static const Color red = Color(0xFFE5001C);
  static const Color grey = Color(0xFFC2C2C2);
  static const Color yellow = Color(0xFFF4C64D);
}
