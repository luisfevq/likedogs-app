import 'package:dogs_lover/app/core/utils/colors.dart';
import 'package:dogs_lover/app/core/widgets/skeleton_home_list.dart';
import 'package:dogs_lover/app/domain/presentation/home/home_controller.dart';
import 'package:dogs_lover/app/domain/presentation/home/widgets/cards_dogs_breed_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BackContent extends StatelessWidget {
  const BackContent({
    Key key,
    this.controller,
  }) : super(key: key);
  final HomeController controller;

  void onTapSearchButton({double width}) {
    controller.changeSearchMenuWidth(width);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => AnimatedContainer(
        duration: const Duration(milliseconds: 300),
        curve: Curves.elasticOut,
        width: Get.width,
        height: Get.height,
        padding: EdgeInsets.only(
          left: controller.widthMenu.value + 30,
          top: 50.0,
          right: 30,
        ),
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Stack(
                children: [
                  Container(
                    width: double.infinity,
                    child: Text(
                      "Dogs Lover",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 25.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    bottom: 0,
                    top: 0,
                    child: GestureDetector(
                      onTap: () {
                        onTapSearchButton(
                            width: controller.widthSearchButton.value);
                      },
                      child: AnimatedContainer(
                        duration: const Duration(milliseconds: 300),
                        width: controller.widthSearchButton.value,
                        height: 40.0,
                        decoration: BoxDecoration(
                          color: ColorDogsLover.orange,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: controller.isSearchButtonExpanded.value
                            ? searchBar()
                            : Center(
                                child: Icon(
                                  Icons.search,
                                  color: Colors.white,
                                ),
                              ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                width: double.infinity,
                height: Get.height - 100,
                child: controller.listBreeds.length == 0
                    ? SkeletonHomeList()
                    : ListView.builder(
                        itemCount: controller.listBreeds.length,
                        itemBuilder: (_, int index) {
                          return CardsDogsBreeds(
                            name: controller.listBreeds[index].breedName,
                            isFavoriteAdded:
                                controller.listBreeds[index].breedFavorite,
                            contoller: controller,
                          );
                        },
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget searchBar() {
    return Container(
      width: double.infinity,
      height: 40,
      padding: const EdgeInsets.only(
        top: 3.0,
        bottom: 3.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 40.0,
            child: Center(
              child: Icon(
                Icons.search,
                color: Colors.white,
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: TextField(
                controller: controller.searchTextController,
                cursorColor: Colors.white,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(
                    bottom: 12,
                    right: 2,
                  ),
                  hintText: "Search",
                  hintStyle: TextStyle(color: ColorDogsLover.grey),
                ),
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
