import 'package:dogs_lover/app/data/repository/local_storage_repository.dart';

class LocalStorageMock extends LocalStorageRepoInterface {
  @override
  Future<List<String>> getListFavoritesBreeds() {
    throw UnimplementedError();
  }

  @override
  Future<bool> removeFavoriteBreed(String breedName) {
    throw UnimplementedError();
  }

  @override
  Future<bool> saveFavoriteBreed(String breedName) => Future.value(true);
}
