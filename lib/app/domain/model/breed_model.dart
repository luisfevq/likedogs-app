class BreedModel {
  BreedModel({
    this.breedName,
    this.breeds,
    this.breedFavorite,
  });

  String breedName;
  bool breedFavorite;
  List<dynamic> breeds;
}
