import 'package:dogs_lover/app/core/utils/colors.dart';
import 'package:flutter/material.dart';

class LoadingModal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned.fill(
      child: Container(
        color: Colors.black.withOpacity(.3),
        child: Center(
          child: CircularProgressIndicator(
            valueColor:
                new AlwaysStoppedAnimation<Color>(ColorDogsLover.orange),
            backgroundColor: Colors.white,
          ),
        ),
      ),
    );
  }
}
