import 'package:flutter/material.dart';
import 'package:loading_skeleton/loading_skeleton.dart';

class SkeletonHomeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (_, int index) {
        return Container(
          margin: const EdgeInsets.only(bottom: 20),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: LoadingSkeleton(
              width: double.infinity,
              height: 70,
            ),
          ),
        );
      },
    );
  }
}
