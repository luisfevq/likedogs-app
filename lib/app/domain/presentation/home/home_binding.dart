import 'package:dogs_lover/app/domain/presentation/home/home_controller.dart';
import 'package:get/get.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => HomeController(
        localStorageInterface: Get.find(),
        apiInterface: Get.find(),
      ),
    );
  }
}
