import 'dart:convert';

BreedDetailModel breedDetailModelFromJson(String str) =>
    BreedDetailModel.fromJson(json.decode(str));

String breedDetailModelToJson(BreedDetailModel data) =>
    json.encode(data.toJson());

class BreedDetailModel {
  BreedDetailModel({
    this.breedName,
    this.breedsImages,
    this.status,
  });

  List<String> breedsImages;
  String status;
  String breedName;

  factory BreedDetailModel.fromJson(Map<String, dynamic> json) =>
      BreedDetailModel(
        breedsImages: json["message"] == null
            ? null
            : List<String>.from(json["message"].map((x) => x)),
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": breedsImages == null
            ? null
            : List<dynamic>.from(breedsImages.map((x) => x)),
        "status": status == null ? null : status,
      };
}
