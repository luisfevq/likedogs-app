import 'package:dogs_lover/app/domain/presentation/main_bindings.dart';
import 'package:dogs_lover/app/domain/routes/app.pages.dart';
import 'package:dogs_lover/app/domain/routes/app.routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: DogsLoverRoutes.home,
      getPages: DogsLoverPages.pages,
      initialBinding: MainBinding(),
    ),
  );
}
