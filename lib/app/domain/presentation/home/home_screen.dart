import 'package:dogs_lover/app/core/widgets/loading_modal.dart';
import 'package:dogs_lover/app/domain/presentation/home/home_controller.dart';
import 'package:dogs_lover/app/domain/presentation/home/widgets/back_content_widget.dart';
import 'package:dogs_lover/app/domain/presentation/home/widgets/left_side_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends GetWidget<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: Obx(
        () => Stack(
          children: [
            BackContent(
              controller: controller,
            ),
            LeftSide(
              controller: controller,
            ),
            controller.isLoading.value ? LoadingModal() : SizedBox.shrink()
          ],
        ),
      ),
    );
  }
}
