import 'package:dogs_lover/app/core/utils/colors.dart';
import 'package:dogs_lover/app/domain/presentation/home/home_controller.dart';
import 'package:dogs_lover/app/domain/routes/app.routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CardsDogsBreeds extends StatelessWidget {
  const CardsDogsBreeds({
    Key key,
    this.name,
    this.isFavoriteAdded = false,
    @required this.contoller,
  }) : super(key: key);

  final String name;
  final bool isFavoriteAdded;
  final HomeController contoller;

  void onTapFavorite() {
    if (!isFavoriteAdded) {
      contoller.addfavoriteBreed(name);
    } else {
      contoller.removefavoriteBreed(name);
    }
  }

  void onTapCard() async {
    await contoller.setBreedSelected(name);
    Get.toNamed(DogsLoverRoutes.dogsBreeds);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapCard,
      child: Container(
        height: 70,
        margin: const EdgeInsets.only(bottom: 20),
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: ColorDogsLover.yellow,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              name.capitalize,
              style: TextStyle(
                fontWeight: FontWeight.w600,
              ),
            ),
            GestureDetector(
              onTap: onTapFavorite,
              child: CircleAvatar(
                radius: 15,
                backgroundColor: Colors.white.withOpacity(.6),
                child: Icon(
                  isFavoriteAdded ? Icons.favorite : Icons.favorite_border,
                  color: !isFavoriteAdded
                      ? ColorDogsLover.orange.withOpacity(.4)
                      : ColorDogsLover.red,
                  size: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
