import 'dart:convert';

BreedListModel breedListModelFromJson(String str) =>
    BreedListModel.fromJson(json.decode(str));

String breedListModelToJson(BreedListModel data) => json.encode(data.toJson());

class BreedListModel {
  BreedListModel({
    this.message,
    this.status,
  });

  Map<String, List<String>> message;
  String status;

  factory BreedListModel.fromJson(Map<String, dynamic> json) => BreedListModel(
        message: json["message"] == null
            ? null
            : Map.from(json["message"]).map((k, v) =>
                MapEntry<String, List<String>>(
                    k, List<String>.from(v.map((x) => x)))),
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null
            ? null
            : Map.from(message).map((k, v) => MapEntry<String, dynamic>(
                k, List<dynamic>.from(v.map((x) => x)))),
        "status": status == null ? null : status,
      };
}
