import 'package:dio/dio.dart';
import 'package:dogs_lover/app/core/utils/api.dart';
import 'package:dogs_lover/app/data/repository/api_repository.dart';
import 'package:dogs_lover/app/domain/model/breed_detail_model.dart';
import 'package:dogs_lover/app/domain/model/breed_list_model.dart';

class ApiImplementation extends ApiRepoInterface {
  @override
  Future<BreedListModel> getListAllBreeds() async {
    Response _response;
    Dio _dio = new Dio();
    BreedListModel _bredList = BreedListModel();

    try {
      _response = await _dio.get(API_BREED + "/breeds/list/all");
      if (_response.statusCode == 200) {
        _bredList = BreedListModel.fromJson(_response.data);
      }
    } catch (e) {}

    return _bredList;
  }

  @override
  Future<BreedDetailModel> getListFromBreedName(String breedName) async {
    Response _response;
    Dio _dio = new Dio();
    BreedDetailModel _bredList = BreedDetailModel();
    try {
      _response = await _dio.get(API_BREED + "/breed/$breedName/images");
      if (_response.statusCode == 200) {
        _bredList = BreedDetailModel.fromJson(_response.data);
      }
    } catch (e) {}

    return _bredList;
  }
}
