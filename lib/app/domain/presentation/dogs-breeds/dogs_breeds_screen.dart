import 'package:dogs_lover/app/core/utils/colors.dart';
import 'package:dogs_lover/app/domain/presentation/dogs-breeds/widgets/card_breed_detail_widget.dart';
import 'package:dogs_lover/app/domain/presentation/home/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DogsBreedsScreen extends GetWidget<HomeController> {
  @override
  Widget build(BuildContext context) {
    final breedModel = controller.breedCurrent.value;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorDogsLover.orange,
        title: Text(
          breedModel.breedName.capitalize,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: breedModel.breedsImages.length,
        itemBuilder: (_, int index) {
          return CardBreedDetailWidget(
            imageUrl: breedModel.breedsImages[index],
          );
        },
      ),
    );
  }
}
