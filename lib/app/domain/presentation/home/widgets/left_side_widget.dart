import 'package:dogs_lover/app/core/utils/colors.dart';
import 'package:dogs_lover/app/domain/presentation/home/home_controller.dart';
import 'package:dogs_lover/app/domain/routes/app.routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LeftSide extends StatelessWidget {
  const LeftSide({Key key, this.controller}) : super(key: key);
  final HomeController controller;

  void onTapFavoriteIcon() {
    Get.toNamed(DogsLoverRoutes.favorites);
  }

  void onDragMenu(DragUpdateDetails details) {
    if (details.delta.dx <= -0.7) {
      controller.changeMenuWidth(Get.width * .1);
      controller.stateMenuExpanded(false);
    } else {
      controller.changeMenuWidth(Get.width * .25);
      controller.stateMenuExpanded(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragUpdate: (details) {
        onDragMenu(details);
      },
      child: Obx(
        () => AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          curve: Curves.elasticOut,
          width: controller.widthMenu.value,
          height: Get.height,
          color: ColorDogsLover.orange,
          child: Column(
            children: [
              Container(
                height: 150,
                child: GestureDetector(
                  onTap: onTapFavoriteIcon,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.favorite,
                        color: Colors.white,
                        size: controller.isMenuExpanded.value ? 40 : 30,
                      ),
                      Text(
                        "Liked",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: 100,
                child: RotatedBox(
                  quarterTurns: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Center(
                          child: Text(
                            "Dogs",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      AnimatedContainer(
                        duration: const Duration(milliseconds: 300),
                        curve: Curves.elasticOut,
                        width: 50,
                        height: controller.isMenuExpanded.value ? 20 : 5,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12),
                            topLeft: Radius.circular(12),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: 100,
                child: RotatedBox(
                  quarterTurns: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Center(
                          child: Text(
                            "Cats",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 50,
                        height: controller.isMenuExpanded.value ? 20 : 5,
                        decoration: BoxDecoration(
                          color: ColorDogsLover.orange,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12),
                            topLeft: Radius.circular(12),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
