import 'package:dogs_lover/app/core/utils/colors.dart';
import 'package:dogs_lover/app/core/widgets/empty_data.dart';
import 'package:dogs_lover/app/core/widgets/loading_modal.dart';
import 'package:dogs_lover/app/domain/presentation/home/home_controller.dart';
import 'package:dogs_lover/app/domain/presentation/home/widgets/cards_dogs_breed_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FavoritesScreen extends GetWidget<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Stack(
        children: [
          Scaffold(
            appBar: AppBar(
              backgroundColor: ColorDogsLover.orange,
              title: Text(
                "Favorites",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              centerTitle: true,
            ),
            body: Container(
              width: Get.width,
              height: Get.height,
              padding: EdgeInsets.only(
                left: 30,
                top: 10.0,
                right: 30,
              ),
              child: controller.listFavoriteBreed.length == 0
                  ? EmptyData(
                      text: "You don't have favorites yet",
                    )
                  : ListView.builder(
                      itemCount: controller.listFavoriteBreed.length,
                      itemBuilder: (BuildContext context, int index) {
                        return CardsDogsBreeds(
                          name: controller.listFavoriteBreed[index],
                          isFavoriteAdded: true,
                          contoller: controller,
                        );
                      },
                    ),
            ),
          ),
          controller.isLoading.value ? LoadingModal() : SizedBox.shrink(),
        ],
      ),
    );
  }
}
