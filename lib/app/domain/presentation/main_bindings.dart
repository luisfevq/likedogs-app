import 'package:dogs_lover/app/data/datasource/api_imp.dart';
import 'package:dogs_lover/app/data/datasource/local_storage_imp.dart';
import 'package:dogs_lover/app/data/repository/api_repository.dart';
import 'package:dogs_lover/app/data/repository/local_storage_repository.dart';
import 'package:get/get.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LocalStorageRepoInterface>(() => LocalStorageImplementation());
    Get.lazyPut<ApiRepoInterface>(() => ApiImplementation());
  }
}
