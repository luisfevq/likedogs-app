import 'package:dogs_lover/app/data/repository/local_storage_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageImplementation extends LocalStorageRepoInterface {
  @override
  Future<List<String>> getListFavoritesBreeds() {
    return _getFavoritesBreeds();
  }

  @override
  Future<bool> removeFavoriteBreed(String breedName) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.remove(breedName);
  }

  @override
  Future<bool> saveFavoriteBreed(String breedName) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> _list = await _getFavoritesBreeds();
    _list.add(breedName);
    return sharedPreferences.setStringList("breeds_list", _list);
  }

  Future<List<String>> _getFavoritesBreeds() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> _list = sharedPreferences.getStringList("breeds_list");
    if (_list == null) {
      return [];
    } else {
      return _list;
    }
  }
}
