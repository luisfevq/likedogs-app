class DogsLoverRoutes {
  static final String home = '/home';
  static final String favorites = '/favorites';
  static final String dogsBreeds = '/dogsBreeds';
}
