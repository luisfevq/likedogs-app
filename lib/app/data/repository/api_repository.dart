import 'package:dogs_lover/app/domain/model/breed_detail_model.dart';
import 'package:dogs_lover/app/domain/model/breed_list_model.dart';

abstract class ApiRepoInterface {
  Future<BreedListModel> getListAllBreeds();
  Future<BreedDetailModel> getListFromBreedName(String breedName);
}
