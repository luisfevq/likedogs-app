abstract class LocalStorageRepoInterface {
  Future<List<String>> getListFavoritesBreeds();
  Future<bool> saveFavoriteBreed(String breedName);
  Future<bool> removeFavoriteBreed(String breedName);
}
