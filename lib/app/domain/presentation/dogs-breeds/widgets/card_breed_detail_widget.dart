import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';

class CardBreedDetailWidget extends StatelessWidget {
  const CardBreedDetailWidget({
    Key key,
    this.imageUrl,
  }) : super(key: key);

  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: 250,
      margin: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 20,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: CachedNetworkImage(
          imageUrl: imageUrl,
          fit: BoxFit.cover,
          placeholder: (_, url) => loadingDog(),
        ),
      ),
    );
  }

  Widget loadingDog() {
    return Image(
      image: AssetImage("assets/loading.gif"),
      fit: BoxFit.cover,
    );
  }
}
