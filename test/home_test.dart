import 'package:dogs_lover/app/domain/model/breed_model.dart';
import 'package:dogs_lover/app/domain/presentation/home/home_controller.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';

import 'api_repository_mock.dart';
import 'local_storage_mock.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  HomeController homeController;

  setUp(() {
    final homeBinding = BindingsBuilder(() {
      Get.lazyPut(
        () => HomeController(
          localStorageInterface: LocalStorageMock(),
          apiInterface: ApiRepositoryMock(),
        ),
      );
    });
    homeBinding.builder();
    homeController = Get.find<HomeController>();
  });
  group("HomeController", () {
    test("Get all breeds list", () {
      homeController.getAllBreeds();
    });
    test("Breed affenpinscher is not favorite", () {
      final result = homeController.isFavoriteBreed("affenpinscher");
      expect(result, false);
    });
    test("Breed bulldogs is favorite", () {
      BreedModel breedModel = new BreedModel();
      breedModel.breedFavorite = true;
      breedModel.breedName = "bulldogs";
      breedModel.breeds = [];
      homeController.listBreeds.add(breedModel);
      homeController.addfavoriteBreed("bulldog");
      homeController.isFavoriteBreed("bulldog");
    });
    test("Validate only breed bulldogs correct added", () {
      BreedModel breedModel = new BreedModel();
      breedModel.breedFavorite = true;
      breedModel.breedName = "bulldogs";
      breedModel.breeds = [];
      homeController.listBreeds.add(breedModel);
      homeController.addfavoriteBreed("bullterrier");
      homeController.isFavoriteBreed("bulldog");
    });
    test("Set Breed Selected - Class", () {});
  });
}
