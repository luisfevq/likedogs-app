# dogs_lover

A new Flutter project.

## Getting Started

<img src="https://bitbucket.org/luisfevq/likedogs-app/raw/main/screen/preview-dogs-lover-50.png"/>

<img src="https://bitbucket.org/luisfevq/likedogs-app/raw/main/screen/dogs-lover-50.gif"/>

### Run project

(main branch)

- Execute command: flutter pub get (into flutter project folder)
- Execute command: flutter run (there must be an emulator / device running) 
- Enjoy

### Test

- In test folder execute command: flutter test

### Functionalities

- List all Breeds
- Add a breed to favorite list
- Shows the detail (list) of images of the breed
- Show list of favorite breed
- Drag/Swipe left menu (Expanded and Collapse) - Animation


API:


Get all Breeds
https://dog.ceo/api/breeds/list/all


Get images of Breed selected
https://dog.ceo/api/breed/bulldog/images

