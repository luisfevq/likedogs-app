import 'package:dogs_lover/app/domain/presentation/dogs-breeds/dogs_breeds_screen.dart';
import 'package:dogs_lover/app/domain/presentation/favorites/favorites_screen.dart';
import 'package:dogs_lover/app/domain/presentation/home/home_binding.dart';
import 'package:dogs_lover/app/domain/presentation/home/home_screen.dart';
import 'package:dogs_lover/app/domain/presentation/main_bindings.dart';
import 'package:dogs_lover/app/domain/routes/app.routes.dart';
import 'package:get/get.dart';

class DogsLoverPages {
  static final pages = [
    GetPage(
      name: DogsLoverRoutes.home,
      page: () => HomeScreen(),
      bindings: [MainBinding(), HomeBinding()],
    ),
    GetPage(
      name: DogsLoverRoutes.favorites,
      page: () => FavoritesScreen(),
      bindings: [MainBinding(), HomeBinding()],
    ),
    GetPage(
      name: DogsLoverRoutes.dogsBreeds,
      page: () => DogsBreedsScreen(),
      bindings: [MainBinding(), HomeBinding()],
    ),
  ];
}
